from utils import Graph, PriorityQueue, mannatan, print_grid, reconstruct_path

def a_star(graph, start, goal, drawfun = None, heuristic = mannatan):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from = dict()
    cost_so_far = dict()
    came_from[start] = None
    cost_so_far[start] = 0
    running = True
    while not frontier.empty() and running:
        current = frontier.get()

        if current == goal:
            break

        for next in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(goal, next)
                frontier.put(next, priority)
                came_from[next] = current
                try : running = drawfun(graph, reconstruct_path(came_from, start, current))
                except Exception as e : print(f"This exception has occured : {e}.")
    return reconstruct_path(came_from, start, current)

def dijkstra(graph, start, goal, drawfun = None):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from = dict()
    cost_so_far = dict()
    came_from[start] = None
    cost_so_far[start] = 0
    running = True
    while not frontier.empty() and running:
        current = frontier.get()

        if current == goal:
            break

        for next in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                frontier.put(next, new_cost)
                came_from[next] = current
                try : running = drawfun(graph, reconstruct_path(came_from, start, current))
                except Exception as e : print(f"This exception has occured : {e}.")
    return reconstruct_path(came_from, start, current)

def bidirectional_search(graph, start, goal, drawfun = None):
    frontier_start = PriorityQueue()
    frontier_goal = PriorityQueue()
    frontier_start.put(start, 0)
    frontier_goal.put(goal, 0)
    came_from_start = dict()
    came_from_goal = dict()
    cost_so_far_start = dict()
    cost_so_far_goal = dict()
    came_from_start[start] = None
    came_from_goal[goal] = None
    cost_so_far_start[start] = 0
    cost_so_far_goal[goal] = 0
    running = True
    while not frontier_start.empty() and not frontier_goal.empty() and running:
        current_start = frontier_start.get()
        current_goal = frontier_goal.get()

        if current_start in came_from_goal:
            break

        for next_start in graph.neighbors(current_start):
            new_cost = cost_so_far_start[current_start] + graph.cost(current_start, next_start)
            if next_start not in cost_so_far_start or new_cost < cost_so_far_start[next_start]:
                cost_so_far_start[next_start] = new_cost
                frontier_start.put(next_start, new_cost)
                came_from_start[next_start] = current_start
                try : running = drawfun(graph, reconstruct_path(came_from_start, start, current_start))
                except Exception as e : print(f"This exception has occured : {e}.")
                
        if current_goal in came_from_start:
            break
            
        for next_goal in graph.neighbors(current_goal):
            new_cost = cost_so_far_goal[current_goal] + graph.cost(current_goal, next_goal)
            if next_goal not in cost_so_far_goal or new_cost < cost_so_far_goal[next_goal]:
                cost_so_far_goal[next_goal] = new_cost
                frontier_goal.put(next_goal, new_cost)
                came_from_goal[next_goal] = current_goal
                try : running = drawfun(graph, reconstruct_path(came_from_goal, goal, current_goal))
                except Exception as e : print(f"This exception has occured : {e}.")
                
    return reconstruct_path(came_from_start, start, current_start) + reconstruct_path(came_from_goal, goal, current_goal)

def create_regions(graph):
    # this is an example of how you could divide the graph into regions
    # you can adjust the size and number of regions according to your needs
    regions = []
    for i in range(0, graph.rows, 10):
        for j in range(0, graph.cols, 10):
            region = [(i, j), (i+10, j+10)]
            regions.append(region)
    return regions

def find_region(regions, point):
    for i in range(len(regions)):
        if point[0] >= regions[i][0][0] and point[0] < regions[i][1][0] and point[1] >= regions[i][0][1] and point[1] < regions[i][1][1]:
            return i
    return None

def local_search(graph, start, goal, drawfun = None):
    # you can use any local search algorithm you want
    return a_star(graph, start, goal, drawfun)

def global_search(graph, start, region, drawfun = None):
    start_point = None
    end_point = None
    for point in graph.neighbors(start):
        if point[0] >= region[0][0] and point[0] <= region[1][0] and point[1] >= region[0][1] and point[1] <= region[1][1]:
            start_point = point
            break
    for point in graph.neighbors(start):
        if point[0] >= region[0][0] and point[0] <= region[1][0] and point[1] >= region[0][1] and point[1] <= region[1][1]:
            end_point = point
            break
    if start_point is None or end_point is None:
        return None
    return bidirectional_search(graph, start_point, end_point, drawfun)




def hpa(graph, start, goal, drawfun = None):
    # create a list of regions
    regions = create_regions(graph)
    # create a list of paths, one for each region
    paths = [None] * len(regions)
    # find the start and goal regions
    start_region = find_region(regions, start)
    goal_region = find_region(regions, goal)

    # check if the start and goal are in the same region
    if start_region == goal_region:
        # use a local search algorithm to find the path
        paths[start_region] = local_search(graph, start, goal, drawfun)
    else:
        # use a global search algorithm to find the path between regions
        paths[start_region] = global_search(graph, start, regions[start_region], drawfun)
        paths[goal_region] = global_search(graph, goal, regions[goal_region], drawfun)
        for i in range(len(regions)):
            if i != start_region and i != goal_region:
                paths[i] = global_search(graph, regions[i][0], regions[i][1], drawfun)

    # combine the paths to create the final path
    final_path = combine_paths(paths, start_region, goal_region)
    return final_path

