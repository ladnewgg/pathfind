import heapq
import random
import numpy as np


class PriorityQueue:
	def __init__(self):
		self.elements = []

	def empty(self):
		return len(self.elements) == 0

	def put(self, item, priority):
		heapq.heappush(self.elements, (priority, item))

	def get(self):
		return heapq.heappop(self.elements)[1]


class Graph:
	def __init__(self, grid):
		self.grid = grid
		self.rows = len(grid)
		self.cols = len(grid[0])

	def findObjective(self, a, b, length, width) :
		found = False
		while not found :
			(x, y) = random.randint(a, a + length), random.randint(b, b + width)
			try :
				if self.grid[x][y] == '.' : found = True
			except Exception as e : print(a, b, length, width, x, y)
		return (x, y)

	def in_bounds(self, id):
		(x, y) = id
		return 0 <= x < self.rows and 0 <= y < self.cols

	def passable(self, id):
		(x, y) = id
		if not (0 <= x < self.rows and 0 <= y < self.cols):
			return False
		return self.grid[x][y] != '#'


	def blocked(self, x, y):
		return not self.passable((x, y))

	def neighbors(self, id):
		(x, y) = id
		results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
		results = filter(self.in_bounds, results)
		results = filter(self.passable, results)
		return results

	def cost(self, current, next):
		return 1

def mannatan(a, b):
	(x1, y1) = a
	(x2, y2) = b
	return abs(x1 - x2) + abs(y1 - y2)

def euclidian(a, b) :
	(x1, y1) = a
	(x2, y2) = b
	return ((x1 - x2)**2 + (y1 - y2)**2)**.5

def reconstruct_path(came_from, start, goal):
	current = goal
	path = [current]
	while current != start:
		current = came_from[current]
		path.append(current)
	path.reverse()
	return path

def is_a_middle_square(row, col, rows, cols, mid) :
	# if row < mid * rows or row > (1 - mid) * rows : return False
	if col < mid * cols or col > (1 - mid) * cols : return False
	return True

def generate_structure(wall, path, p) :
	if random.random() < p : return wall
	return path

def create_grid(rows, cols, p = 0.2, mp = 0.37, mid = 0.1):
	grid = []
	for i in range(rows) :
		col = []
		for j in  range(cols) :
			if is_a_middle_square(i, j, rows, cols, mid) : col.append(generate_structure('#', '.', mp))
			else : col.append(generate_structure('#', '.', p))
		grid.append(col)
	return grid

def print_grid(graph, path=[]):
	for row in range(graph.rows):
		print("    ", end="")
		for col in range(graph.cols):
			if (row, col) in path:
				print("O", end="")
			elif not graph.passable((row, col)):
				print("#", end="")
			else:
				print(".", end="")
		print()

def create_labyrinth(n):
	# Create an n*n matrix filled with walls (1)
	labyrinth = np.ones((n, n), dtype=int)
	# Choose a random starting point
	start_x, start_y = np.random.randint(0, n), np.random.randint(0, n)
	# Carve a path through the matrix using depth-first search
	stack = [(start_x, start_y)]
	while stack:
		x, y = stack.pop()
		labyrinth[x, y] = 0
		neighbors = []
		if x > 0 and labyrinth[x - 1, y] == 1:
			neighbors.append((x - 1, y))
		if x < n - 1 and labyrinth[x + 1, y] == 1:
			neighbors.append((x + 1, y))
		if y > 0 and labyrinth[x, y - 1] == 1:
			neighbors.append((x, y - 1))
		if y < n - 1 and labyrinth[x, y + 1] == 1:
			neighbors.append((x, y + 1))
		if neighbors:
			stack.append((x, y))
			x, y = neighbors[np.random.randint(0, len(neighbors))]
			stack.append((x, y))
	# Convert the matrix to a string matrix
	labyrinth = np.vectorize(lambda x: "#" if x else ".")(labyrinth)
	return labyrinth.tolist()