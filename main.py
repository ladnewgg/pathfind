import pygame
from utils import create_grid, Graph, reconstruct_path, create_labyrinth, euclidian
from algo import a_star, dijkstra, bidirectional_search, hpa
from labyrinthe import lab2

OFFSET_TOP = 50
OFFSET_BOTTOM = 50
BACKGROUND = (0, 0, 0)
OBSTACLE = (135, 128, 145)
PATH_COLOR = (255, 255, 250)
OLD_PATH_COLOR = (70, 50, 80)
START = (0, 255, 0)
GOAL = (255, 0, 0)

old_path = None
pygame.init()
font = pygame.font.Font('assets/Roboto.ttf', 30)

def is_quiting(events) :
    for event in events:
        if event.type == pygame.QUIT:
            return False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                 return False
    return True


def rectangle(screen, x, y, length, width, color) :
    rect = pygame.Rect(x, y, length, width)
    pygame.draw.rect(screen, color, rect)

def Render_Text(what, color, where):
    global font
    text = font.render(what, 1, pygame.Color(color))
    screen.blit(text, where)

def drawPath(graph, path, limit = 0):
    global running
    global old_path
    running = is_quiting(pygame.event.get())

    if old_path is not None:
        for (row, col) in old_path:
            rectangle(screen, col*cell_size + OFFSET_BOTTOM, row*cell_size + OFFSET_TOP, cell_size, cell_size, OLD_PATH_COLOR)
    for (row, col) in path:
        rectangle(screen, col*cell_size + OFFSET_BOTTOM, row*cell_size + OFFSET_TOP, cell_size, cell_size, PATH_COLOR)

    rectangle(screen, start[1]*cell_size + OFFSET_BOTTOM, start[0]*cell_size + OFFSET_TOP, cell_size, cell_size, START)
    rectangle(screen, goal[1]*cell_size + OFFSET_BOTTOM, goal[0]*cell_size + OFFSET_TOP, cell_size, cell_size, GOAL)

    old_path = path
    pygame.display.update()

    if limit != 0 : clock.tick(60) ; print(limit)
    return running

grid_size = (300, 400)
grid = create_grid(grid_size[0], grid_size[1], 0.1, 0.4, 0.1)
graph = Graph(grid)

# start = graph.findObjective(1, 50, 5, grid_size[1] - 50)
# goal = graph.findObjective(grid_size[0] - 6, 50, 5, grid_size[1] - 50)

start = graph.findObjective(50, 1, grid_size[0] - 50, 5)
goal = graph.findObjective(50, grid_size[1] - 6, grid_size[0] - 50, 5)


cell_size = 2
size = (grid_size[1] * cell_size + 2 * OFFSET_BOTTOM, grid_size[0] * cell_size + 2 * OFFSET_TOP)
screen = pygame.display.set_mode(size)

# Loop until the user clicks the close button
running = True

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

screen.fill(BACKGROUND)
for row in range(graph.rows):
    for col in range(graph.cols):
        rect = pygame.Rect(col*cell_size + OFFSET_TOP, row*cell_size + OFFSET_BOTTOM, cell_size, cell_size)
        if not graph.passable((row, col)):
            pygame.draw.rect(screen, OBSTACLE, rect)
        else:
            pygame.draw.rect(screen, BACKGROUND, rect)


# drawPath(graph, a_star(graph, start, goal, drawPath))
# drawPath(graph, dijkstra(graph, start, goal, drawPath))
drawPath(graph, bidirectional_search(graph, start, goal, drawPath))
# drawPath(graph, hpa(graph, start, goal, drawPath))
while running :
    running = is_quiting(pygame.event.get())
    clock.tick(60)
pygame.quit()